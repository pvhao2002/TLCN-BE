package com.app.toeic.enums;

public enum ERole {
    SUPERADMIN, ADMIN, USER
}
