package com.app.toeic.enums;

public enum EUser {
    ACTIVE, INACTIVE, BLOCKED
}
