package com.app.toeic.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class BearerToken implements Serializable {
    private String accessToken ;
    private String tokenType ;
}
