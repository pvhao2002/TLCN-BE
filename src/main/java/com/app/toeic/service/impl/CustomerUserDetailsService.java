package com.app.toeic.service.impl;

import com.app.toeic.model.UserAccount;
import com.app.toeic.repository.IUserAccountRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class CustomerUserDetailsService implements UserDetailsService {

    private final IUserAccountRepository iUserRepository;
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return iUserRepository
                .findByUsername(username)
                .orElseThrow(() -> new UsernameNotFoundException("User not found !"));
    }


}